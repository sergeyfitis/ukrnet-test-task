package com.sergeyfitis.ukrnettest.presentation.screens.login;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.presentation.base.BasePresenter;
import com.sergeyfitis.ukrnettest.presentation.base.BaseView;

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        void onLoggedIn(@NonNull String email);
        void loginFailed(@NonNull String message);
        void invalidEmail(@NonNull String message);
        void invalidPassword(@NonNull String message);
        void showPseudoCaptcha();
    }

    abstract class Presenter extends BasePresenter<View> {
        abstract void login(@NonNull String email, @NonNull String password);
        abstract void submitPseudoCaptcha(@NonNull String email, @NonNull String password, @NonNull String code);
    }
}
