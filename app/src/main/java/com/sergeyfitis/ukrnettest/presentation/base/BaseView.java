package com.sergeyfitis.ukrnettest.presentation.base;

public interface BaseView<T extends BasePresenter> {
    void showProgressDialog();
    void hideProgressDialog();
}
