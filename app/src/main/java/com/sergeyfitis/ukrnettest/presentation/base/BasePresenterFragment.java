package com.sergeyfitis.ukrnettest.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BasePresenterFragment<P extends BasePresenter<V>, V extends BaseView<P>> extends Fragment {

    @NonNull
    private P presenter;

    @NonNull
    protected abstract P createPresenter();
    @NonNull
    protected abstract V getPresentationView();
    @LayoutRes
    protected abstract int getLayoutId();

    @NonNull
    protected P getPresenter() {
        return presenter;
    }

    @CallSuper
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(getPresentationView());
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        presenter.detachView();
        super.onDestroyView();
    }

    @CallSuper
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = createPresenter();
    }
}
