package com.sergeyfitis.ukrnettest.presentation.screens.main;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.domain.source.DataSource;

public class MainPresenter extends MainContract.Presenter {

    @NonNull
    private final DataSource repository;

    MainPresenter(@NonNull DataSource repository) {
        this.repository = repository;
    }

    @Override
    void loadUser(@NonNull String email) {
        showProgress();
        repository.getUserNumber(email, new DataSource.LoadUserCallback() {
            @Override
            public void onUserNumberLoaded(int number) {
                hideProgress();
                if (getView() != null) {
                    getView().onUserNumberLoaded(number);
                }
            }

            @Override
            public void onUserNotFound() {
                hideProgress();
                if (getView() != null) {
                    getView().onUserNotFound();
                }
            }
        });
    }
}
