package com.sergeyfitis.ukrnettest.presentation.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class BasePresenter<T extends BaseView> {
    @Nullable
    private T view;

    protected void showProgress() {
        if (getView() != null) {
            getView().showProgressDialog();
        }
    }

    protected void hideProgress() {
        if (getView() != null) {
            getView().hideProgressDialog();
        }
    }

    public void attachView(@NonNull T view) {
        if (this.view != null) throw new IllegalStateException("View already attached");
        this.view = view;
    }

    public void detachView() {
        view = null;
    }

    @Nullable
    public T getView() {
        return view;
    }
}
