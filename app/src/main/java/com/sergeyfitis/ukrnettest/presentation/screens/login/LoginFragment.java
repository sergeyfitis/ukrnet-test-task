package com.sergeyfitis.ukrnettest.presentation.screens.login;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputLayout;
import com.sergeyfitis.ukrnettest.R;
import com.sergeyfitis.ukrnettest.data.resources.ResourcesProvider;
import com.sergeyfitis.ukrnettest.data.resources.ResourcesProviderImpl;
import com.sergeyfitis.ukrnettest.data.source.UserRepositoryImpl;
import com.sergeyfitis.ukrnettest.data.source.local.LocalDataSourceImpl;
import com.sergeyfitis.ukrnettest.data.source.remote.RemoteDataSourceImpl;
import com.sergeyfitis.ukrnettest.data.validators.EmailValidator;
import com.sergeyfitis.ukrnettest.data.validators.PasswordValidator;
import com.sergeyfitis.ukrnettest.helpers.DialogHelpers;
import com.sergeyfitis.ukrnettest.helpers.InputMethodHelper;
import com.sergeyfitis.ukrnettest.presentation.base.BasePresenterFragment;

public class LoginFragment extends BasePresenterFragment<LoginContract.Presenter, LoginContract.View> implements LoginContract.View {

    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;
    private View progressLayout;


    public LoginFragment() {
    }


    @NonNull
    @Override
    protected LoginContract.Presenter createPresenter() {
        ResourcesProvider resourceProvider = new ResourcesProviderImpl(requireContext().getApplicationContext());
        return  new LoginPresenter(
                UserRepositoryImpl.getInstance(LocalDataSourceImpl.getInstance(), RemoteDataSourceImpl.getInstance()),
                new EmailValidator(resourceProvider),
                new PasswordValidator(resourceProvider)
        );
    }

    @NonNull
    @Override
    protected LoginContract.View getPresentationView() {
        return this;
    }

    @LayoutRes
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnLogin = view.findViewById(R.id.btnLogin);

        tilEmail = view.findViewById(R.id.tilEmail);
        tilPassword = view.findViewById(R.id.tilPassword);
        progressLayout = view.findViewById(R.id.flProgress);

        btnLogin.setOnClickListener(v -> {
            InputMethodHelper.hideKeyboard(view.findFocus());
            clearErrors();
            getPresenter().login(
                    tilEmail.getEditText().getText().toString(),
                    tilPassword.getEditText().getText().toString()
            );
        });
    }

    @Override
    public void showProgressDialog() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressDialog() {
        progressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onLoggedIn(@NonNull String email) {
        LoginFragmentDirections.ActionLoginFragmentToMainFragment toMainFragmentAction =
                LoginFragmentDirections.actionLoginFragmentToMainFragment(email);
        NavController navController = Navigation.findNavController(requireActivity(), R.id.navHostFragment);
        navController.navigate(toMainFragmentAction);
    }

    @Override
    public void loginFailed(@NonNull String message) {
        DialogHelpers.showAlertDialog(requireContext(), getString(R.string.login_failed), message, android.R.string.ok);
    }

    @Override
    public void invalidEmail(@NonNull String message) {
        tilEmail.setError(message);
        tilEmail.setErrorEnabled(true);
    }

    @Override
    public void invalidPassword(@NonNull String message) {
        tilPassword.setError(message);
        tilPassword.setErrorEnabled(true);
    }

    @Override
    public void showPseudoCaptcha() {
        DialogHelpers.showPseudoCaptcha(requireContext(), code -> {
            getPresenter().submitPseudoCaptcha(
                    tilEmail.getEditText().getText().toString(),
                    tilPassword.getEditText().getText().toString(),
                    code
            );
        });
    }

    private void clearErrors() {
        tilEmail.setErrorEnabled(false);
        tilPassword.setErrorEnabled(false);
    }
}
