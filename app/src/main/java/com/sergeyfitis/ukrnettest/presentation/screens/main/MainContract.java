package com.sergeyfitis.ukrnettest.presentation.screens.main;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.presentation.base.BasePresenter;
import com.sergeyfitis.ukrnettest.presentation.base.BaseView;

public interface MainContract {
    interface View extends BaseView<Presenter> {
        void onUserNumberLoaded(int number);
        void onUserNotFound();
    }

    abstract class Presenter extends BasePresenter<View> {
       abstract void loadUser(@NonNull String email);
    }
}
