package com.sergeyfitis.ukrnettest.presentation.screens.main;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.sergeyfitis.ukrnettest.R;
import com.sergeyfitis.ukrnettest.data.source.UserRepositoryImpl;
import com.sergeyfitis.ukrnettest.data.source.local.LocalDataSourceImpl;
import com.sergeyfitis.ukrnettest.data.source.remote.RemoteDataSourceImpl;
import com.sergeyfitis.ukrnettest.helpers.FibonacciEvaluator;
import com.sergeyfitis.ukrnettest.presentation.base.BasePresenterFragment;

public class MainFragment extends BasePresenterFragment<MainContract.Presenter, MainContract.View> implements MainContract.View {

    private TextView tvUserInfo;
    private View progressLayout;

    public MainFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    protected MainContract.Presenter createPresenter() {
        return new MainPresenter(
                UserRepositoryImpl.getInstance(
                        LocalDataSourceImpl.getInstance(),
                        RemoteDataSourceImpl.getInstance())
        );
    }

    @NonNull
    @Override
    protected MainContract.View getPresentationView() {
        return this;
    }

    @LayoutRes
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnLogout = view.findViewById(R.id.btnLogout);
        tvUserInfo = view.findViewById(R.id.tvInfo);
        progressLayout = view.findViewById(R.id.flProgress);

        String userEmail = MainFragmentArgs.fromBundle(getArguments()).getUserEmail();
        getPresenter().loadUser(userEmail);

        btnLogout.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(requireActivity(), R.id.navHostFragment);
            navController.popBackStack();
        });
    }

    @Override
    public void onUserNumberLoaded(int number) {
        tvUserInfo.setText(number + ":" + FibonacciEvaluator.evaluate(number));
    }

    @Override
    public void onUserNotFound() {
        tvUserInfo.setText(R.string.error_user_not_found);
    }

    @Override
    public void showProgressDialog() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressDialog() {
        progressLayout.setVisibility(View.INVISIBLE);
    }
}
