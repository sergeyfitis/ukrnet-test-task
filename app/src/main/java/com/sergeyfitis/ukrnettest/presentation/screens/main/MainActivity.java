package com.sergeyfitis.ukrnettest.presentation.screens.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.sergeyfitis.ukrnettest.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
