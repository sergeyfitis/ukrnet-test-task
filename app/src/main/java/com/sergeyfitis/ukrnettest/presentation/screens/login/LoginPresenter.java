package com.sergeyfitis.ukrnettest.presentation.screens.login;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.domain.source.DataSource;
import com.sergeyfitis.ukrnettest.data.validators.ValidationResult;
import com.sergeyfitis.ukrnettest.data.validators.Validator;

public class LoginPresenter extends LoginContract.Presenter {

    @NonNull
    private final DataSource repository;
    @NonNull
    private final Validator<String> emailValidator;
    @NonNull
    private final Validator<String> passwordValidator;

    LoginPresenter(@NonNull DataSource repository,
                   @NonNull Validator<String> emailValidator,
                   @NonNull Validator<String> passwordValidator) {
        this.repository = repository;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    @Override
    public void login(@NonNull String email, @NonNull String password) {

        ValidationResult emailValidationResult = emailValidator.validate(email);
        ValidationResult passwordValidationResult = passwordValidator.validate(password);

        if (emailValidationResult.isValid() && passwordValidationResult.isValid()) {
            showProgress();
            repository.login(email, password, new DataSource.LoginCallback() {
                @Override
                public void onUserLoggedIn(@NonNull String email) {
                    hideProgress();
                    if (getView() != null) {
                        getView().onLoggedIn(email);
                    }
                }

                @Override
                public void onLoginFailed(String errorMessage) {
                    hideProgress();
                    if (getView() != null) {
                        getView().loginFailed(errorMessage);
                    }
                }

                @Override
                public void requestToPromptCaptcha() {
                    hideProgress();
                    if (getView() != null) {
                        getView().showPseudoCaptcha();
                    }
                }
            });
        } else {
            if (getView() != null) {
                if (!emailValidationResult.isValid()) {
                    getView().invalidEmail(emailValidationResult.getErrorMessage());
                }
                if (!passwordValidationResult.isValid()) {
                    getView().invalidPassword(passwordValidationResult.getErrorMessage());
                }
            }
        }
    }

    @Override
    public void submitPseudoCaptcha(@NonNull String email, @NonNull String password, @NonNull String code) {
        repository.enterCaptcha(code, () -> login(email, password));
    }
}
