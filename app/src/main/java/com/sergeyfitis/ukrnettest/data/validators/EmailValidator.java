package com.sergeyfitis.ukrnettest.data.validators;

import android.util.Patterns;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.R;
import com.sergeyfitis.ukrnettest.data.resources.ResourcesProvider;

public class EmailValidator extends Validator<String> {

    @NonNull
    private final ResourcesProvider resourceProvider;

    public EmailValidator(@NonNull ResourcesProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }


    @NonNull
    @Override
    public ValidationResult validate(@NonNull String data) {
        boolean isEmailEmpty = data.isEmpty();
        boolean isEmailValid = Patterns.EMAIL_ADDRESS.matcher(data).matches();
        return isEmailValid ?
                ValidationResult.success() :
                ValidationResult.error(isEmailEmpty ?
                        resourceProvider.stringRes(R.string.error_empty_email) :
                        resourceProvider.stringRes(R.string.error_invalid_email));
    }
}
