package com.sergeyfitis.ukrnettest.data.validators;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.R;
import com.sergeyfitis.ukrnettest.data.resources.ResourcesProvider;

public class PasswordValidator extends Validator<String> {

    @NonNull
    private final ResourcesProvider resourceProvider;

    public PasswordValidator(@NonNull ResourcesProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    @NonNull
    @Override
    public ValidationResult validate(@NonNull String data) {
        boolean isLengthValid = data.length() >= 6;
        return isLengthValid ?
                ValidationResult.success() :
                ValidationResult.error(resourceProvider.stringRes(R.string.error_password_short));
    }
}
