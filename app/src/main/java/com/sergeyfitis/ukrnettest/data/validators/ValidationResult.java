package com.sergeyfitis.ukrnettest.data.validators;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ValidationResult {
    private enum Status {SUCCESS, ERROR}

    @Nullable
    private String errorMessage;
    private Status status;

    private ValidationResult(Status status, @Nullable String errorMessage) {
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public static ValidationResult success() {
        return new ValidationResult(Status.SUCCESS, null);
    }

    public static ValidationResult error(@NonNull String message) {
        return new ValidationResult(Status.ERROR, message);
    }

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isValid() {
        return status == Status.SUCCESS;
    }
}
