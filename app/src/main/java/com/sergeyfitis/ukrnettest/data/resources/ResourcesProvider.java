package com.sergeyfitis.ukrnettest.data.resources;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public interface ResourcesProvider {
    @NonNull
    String stringRes(@StringRes int resId);
}
