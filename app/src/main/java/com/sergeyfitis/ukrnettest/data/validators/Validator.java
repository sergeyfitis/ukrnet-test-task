package com.sergeyfitis.ukrnettest.data.validators;

import androidx.annotation.NonNull;

public abstract class Validator<T> {
    @NonNull
    public abstract ValidationResult validate(@NonNull T data);
}
