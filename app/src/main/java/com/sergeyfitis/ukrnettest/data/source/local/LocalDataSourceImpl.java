package com.sergeyfitis.ukrnettest.data.source.local;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.domain.source.LocalDataSource;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LocalDataSourceImpl implements LocalDataSource {

    private static LocalDataSource instance;

    private final Map<String, Integer> users = Collections.synchronizedMap(new HashMap<>());

    public static synchronized LocalDataSource getInstance() {
        if (instance == null) {
            instance = new LocalDataSourceImpl();
        }
        return instance;
    }

    @Override
    public void login(@NonNull String email, @NonNull String password, @NonNull LoginCallback callback) {
        throw new RuntimeException("No-op");
    }

    @Override
    public void getUserNumber(@NonNull String email, @NonNull LoadUserCallback callback) {
        Integer number = users.get(email);
        if (number == null) {
            callback.onUserNotFound();
        } else {
            callback.onUserNumberLoaded(number);
        }
    }

    @Override
    public void enterCaptcha(@NonNull String code, @NonNull CaptchaCallback callback) {
        throw new RuntimeException("No-op");
    }

    @Override
    public void saveUser(String email) {
        users.put(email, randomNumber(email));
    }

    private int randomNumber(String email) {
        return new Random(email.hashCode()).nextInt(90);
    }

}
