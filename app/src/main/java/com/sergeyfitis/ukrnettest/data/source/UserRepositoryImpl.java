package com.sergeyfitis.ukrnettest.data.source;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.domain.source.LocalDataSource;
import com.sergeyfitis.ukrnettest.domain.source.DataSource;

public class UserRepositoryImpl implements DataSource {
    private static DataSource instance;

    @NonNull
    private final LocalDataSource localDataSource;
    @NonNull
    private final DataSource remoteDataSource;

    private UserRepositoryImpl(@NonNull LocalDataSource localDataSource, @NonNull DataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    public static synchronized DataSource getInstance(@NonNull LocalDataSource localDataSource, @NonNull DataSource remoteDataSource) {
        if (instance == null) {
            instance = new UserRepositoryImpl(localDataSource, remoteDataSource);
        }
        return instance;
    }

    @Override
    public void login(@NonNull String email, @NonNull String password, @NonNull LoginCallback callback) {
        remoteDataSource.login(email, password, new LoginCallback() {
            @Override
            public void onUserLoggedIn(@NonNull String email) {
                callback.onUserLoggedIn(email);
                localDataSource.saveUser(email);
            }

            @Override
            public void onLoginFailed(String errorMessage) {
                callback.onLoginFailed(errorMessage);
            }

            @Override
            public void requestToPromptCaptcha() {
                callback.requestToPromptCaptcha();
            }
        });
    }

    @Override
    public void getUserNumber(@NonNull String email, @NonNull LoadUserCallback callback) {
        localDataSource.getUserNumber(email, callback);
    }

    @Override
    public void enterCaptcha(@NonNull String code, @NonNull CaptchaCallback callback) {
        remoteDataSource.enterCaptcha(code, callback);
    }
}
