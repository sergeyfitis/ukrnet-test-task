package com.sergeyfitis.ukrnettest.data.source.remote;

import androidx.annotation.NonNull;

import com.sergeyfitis.ukrnettest.domain.source.DataSource;
import com.sergeyfitis.ukrnettest.helpers.HandlerHelper;

import java.util.concurrent.TimeUnit;

public class RemoteDataSourceImpl implements DataSource {

    private static final String INVALID_EMAIL = "test@gmail.com";
    private static final String INVALID_PASSWORD = "qwertyqwerty";
    private static final String USER_NOT_EXISTS_EMAIL = "test@ukr.net";
    private static final String USER_NOT_EXISTS_PASSWORD = "12345678";

    private static final int INVALID_ENTERS_LIMIT = 2;
    private static final int INVALID_ENTERS_RATE_LIMIT = 2; // 2 invalid enters try per 15 seconds

    private static DataSource instance;

    private int invalidEntersCounter = 0;
    private int invalidEntersLimitCounter = 0;

    private final Runnable clearInvalidEntersLimitCounter = () -> invalidEntersLimitCounter = 0;

    public static synchronized DataSource getInstance() {
        if (instance == null) {
            instance = new RemoteDataSourceImpl();
        }
        return instance;
    }

    @Override
    public void login(@NonNull String email, @NonNull String password, @NonNull LoginCallback callback) {
        if (shouldPromptCaptcha()) {
            clearInvalidEntersLimitCounter.run();
            callback.requestToPromptCaptcha();
        } else if (email.equalsIgnoreCase(INVALID_EMAIL) && password.equalsIgnoreCase(INVALID_PASSWORD)) {
            HandlerHelper.postDelayed(
                    () -> {
                        callback.onLoginFailed("Login failed. Invalid email or password");
                        incrementInvalidEnterCounter();
                    },
                    TimeUnit.SECONDS.toMillis(1) // simulate network connection delay
            );
        } else if (email.equalsIgnoreCase(USER_NOT_EXISTS_EMAIL) && password.equalsIgnoreCase(USER_NOT_EXISTS_PASSWORD)) {
            HandlerHelper.postDelayed(
                    () -> {
                        callback.onLoginFailed("Login failed. User does not exist");
                        incrementInvalidEnterCounter();
                    },
                    TimeUnit.SECONDS.toMillis(1) // simulate network connection delay
            );
        } else {
            HandlerHelper.postDelayed(
                    () -> callback.onUserLoggedIn(email),
                    TimeUnit.SECONDS.toMillis(1)
            );
        }
    }

    @Override
    public void getUserNumber(@NonNull String email, @NonNull LoadUserCallback callback) {
        throw new RuntimeException("No-op");
    }

    @Override
    public void enterCaptcha(@NonNull String code, @NonNull CaptchaCallback callback) {
        callback.onCaptchaAccepted();
    }

    private void incrementInvalidEnterCounter() {
        invalidEntersCounter += 1;
        if (invalidEntersCounter % INVALID_ENTERS_LIMIT == 0) {
            invalidEntersLimitCounter += 1;
            HandlerHelper.removeDelayedMessage(clearInvalidEntersLimitCounter);
            HandlerHelper.postDelayed(clearInvalidEntersLimitCounter, TimeUnit.SECONDS.toMillis(15));
        }
    }

    private boolean shouldPromptCaptcha() {
        return invalidEntersLimitCounter >= INVALID_ENTERS_RATE_LIMIT;
    }
}
