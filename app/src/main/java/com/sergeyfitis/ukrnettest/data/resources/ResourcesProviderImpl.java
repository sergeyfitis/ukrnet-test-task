package com.sergeyfitis.ukrnettest.data.resources;

import android.content.Context;

import androidx.annotation.NonNull;

public class ResourcesProviderImpl implements ResourcesProvider {

    @NonNull
    private final Context application;

    public ResourcesProviderImpl(@NonNull Context application) {
        this.application = application;
    }

    @NonNull
    @Override
    public String stringRes(int resId) {
        return application.getResources().getString(resId);
    }
}
