package com.sergeyfitis.ukrnettest.helpers;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class InputMethodHelper {
    private InputMethodHelper() {
    }

    public static void hideKeyboard(@Nullable View focusedView) {
        if (focusedView == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) focusedView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    }
}
