package com.sergeyfitis.ukrnettest.helpers;

public final class FibonacciEvaluator {

    private FibonacciEvaluator() {
    }

    public static long evaluate(int n) {
        if (n <= 40) {
            return formula(n);
        }
        long[][] f = matrixMultiplication(n);
        return f[0][0];
    }

    // Fibonacci mathematical formula Fn = {[(√5 + 1)/2] ^ n} / √5
    // Time Complexity O(1)
    private static long formula(int n) {
        double phi = (1 + Math.sqrt(5)) / 2;
        return Math.round(Math.pow(phi, n) / Math.sqrt(5));
    }

    // Time Complexity: O(log n)
    private static long[][] matrixMultiplication(int n) {
        long[][] f = new long[][]{{1, 1}, {1, 0}};
        power(f, n - 1);
        return f;
    }

    private static void multiply(long[][] f, long[][] m) {
        long x = f[0][0] * m[0][0] + f[0][1] * m[1][0];
        long y = f[0][0] * m[0][1] + f[0][1] * m[1][1];
        long z = f[1][0] * m[0][0] + f[1][1] * m[1][0];
        long w = f[1][0] * m[0][1] + f[1][1] * m[1][1];

        f[0][0] = x;
        f[0][1] = y;
        f[1][0] = z;
        f[1][1] = w;
    }

    private static void power(long[][] f, int n) {
        if (n == 0 || n == 1)
            return;
        long[][] m = new long[][]{{1, 1}, {1, 0}};

        power(f, n / 2);
        multiply(f, f);

        if (n % 2 != 0)
            multiply(f, m);
    }
}
