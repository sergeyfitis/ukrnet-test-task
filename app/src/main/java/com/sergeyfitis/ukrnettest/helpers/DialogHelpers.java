package com.sergeyfitis.ukrnettest.helpers;

import android.content.Context;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;

import com.sergeyfitis.ukrnettest.R;

public final class DialogHelpers {
    private DialogHelpers() {
    }

    @MainThread
    public static void showAlertDialog(@NonNull Context ctx,
                         @NonNull String title,
                         @NonNull String message,
                         @StringRes int positiveButton) {
        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButton, null)
                .create()
                .show();
    }

    public static void showPseudoCaptcha(@NonNull Context ctx, Consumer<String> callback) {
        new AlertDialog.Builder(ctx)
                .setTitle(R.string.pseudo_captcha_title)
                .setMessage(R.string.pseudo_captcha_message)
                .setPositiveButton(R.string.btn_submit, (dialog, which) -> callback.accept("Some code entered"))
                .create()
                .show();
    }
}
