package com.sergeyfitis.ukrnettest.helpers;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

public final class HandlerHelper {
    private static final Handler HANDLER = new Handler(Looper.getMainLooper());

    private HandlerHelper() {
    }

    public static void removeDelayedMessage(@NonNull Runnable runnable) {
        HANDLER.removeCallbacks(runnable);
    }

    public static void postDelayed(@NonNull Runnable runnable, long delay) {
        HANDLER.postDelayed(runnable, delay);
    }
}
