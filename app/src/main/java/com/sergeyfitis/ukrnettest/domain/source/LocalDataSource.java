package com.sergeyfitis.ukrnettest.domain.source;

import com.sergeyfitis.ukrnettest.domain.source.DataSource;

public interface LocalDataSource extends DataSource {
    void saveUser(String email);
}
