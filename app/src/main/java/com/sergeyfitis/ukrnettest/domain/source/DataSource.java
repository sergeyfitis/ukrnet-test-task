package com.sergeyfitis.ukrnettest.domain.source;

import androidx.annotation.NonNull;

public interface DataSource {
    interface LoginCallback {
        void onUserLoggedIn(@NonNull String email);
        void onLoginFailed(String errorMessage);
        void requestToPromptCaptcha();
    }

    interface LoadUserCallback {
        void onUserNumberLoaded(int number);
        void onUserNotFound();
    }

    interface CaptchaCallback {
        void onCaptchaAccepted();
    }

    void login(@NonNull String email, @NonNull String password, @NonNull LoginCallback callback);
    void getUserNumber(@NonNull String email, @NonNull LoadUserCallback callback);
    void enterCaptcha(@NonNull String code, @NonNull CaptchaCallback callback);
}
