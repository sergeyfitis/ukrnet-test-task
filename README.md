# UrkNet test task

### Login error test cases

 - Invalid email & password: ```login=test@gmail.com``` ```password=qwertyqwerty```
 - User does not exist: ```login=test@ukr.net``` ```password=12345678```
 - Captcha dialog will be requested after 4 invalid attempts to login within 15 sec time window

Made by Serhii Yaremych ©